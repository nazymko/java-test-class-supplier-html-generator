function isFullyQalifiedClassName(targetClassName) {
    return targetClassName.indexOf(".") > 0;
}

function copyToClip(str) {

    function listener(e) {
        e.clipboardData.setData("text/html", str);
        e.clipboardData.setData("text/plain", str);
        e.preventDefault();
    }

    document.addEventListener("copy", listener);
    document.execCommand("copy");
    document.removeEventListener("copy", listener);
}

function isPrimitiveType(listType) {
    return primitiveTypes.indexOf(listType) > -1;
}

function createWrapper(targetClassName = "ClassNameNotSet",
                       fieldList,
                       container, buildStrategy = "builder",
                       namespace = "",
                       replaceWithList = false,
) {

    let haveListType = fieldList.indexOf("List<") > -1;
    if (haveListType) { // include list imports
        container.add("import java.util.Collections; ");
        container.add("import java.util.List; ");
        container.add("import java.util.Arrays; ");
        container.add("import java.util.stream.Collectors; ");
        container.add("");

    }
    let haveInstantType = fieldList.indexOf("Instant") > -1;

    if (haveInstantType) {
        container.add("import java.time.Instant;");
    }

    if (fieldList.indexOf("BigDecimal") > -1 || fieldList.indexOf("BigInteger") > -1) {
        container.add("import java.math.*; ");
    }

    if (fieldList.indexOf("LocalDateTime") > -1) {
        container.add("import java.time.LocalDateTime; ");
    }

    if (fieldList.indexOf("LocalDate") > -1) {
        container.add("import java.time.LocalDate;");
    }

    container.add("import java.util.function.Supplier; ");
    container.add("import org.assertj.core.api.Assertions;");
    container.add("");

    if (haveListType) {
        container.add("import lombok.Singular; ");
    }
    container.add("import lombok.Data; ");
    container.add("import lombok.Builder; ");
    container.add("");
    if (isFullyQalifiedClassName(targetClassName)) {
        container.add("import " + targetClassName + "; ");
    }

    let className

    /*BEGIN class name with package BEGIN */
    if (isFullyQalifiedClassName(targetClassName)) {
        className = targetClassName.substring(targetClassName.lastIndexOf(".") + 1);
    } else {
        className = targetClassName;
    }
    /*END class name with package END*/

    container.add("");
    container.add("/**");
    container.add(" * Utility class to assemble {@link " + className + "} object.");
    container.add(" */");
    container.add("");
    container.add("@Data ");
    container.add("@Builder ");

    let wrapperClass = className + "Supplier";
    let wrapperWithNamespace = namespace + wrapperClass;
    let builderClass = wrapperWithNamespace + "Builder";

    /*BEGIN class BEGIN*/
    container.add("public class " + wrapperWithNamespace + " implements Supplier<" + className + "> {");

    let fieldListArray = fieldList.split("\n").filter(line => {
        return !(line.startsWith("@") || line.startsWith("//") || line.startsWith("/*"));
    });
    fieldListArray.forEach((line, index, v2, v3) => {
        let field = line.trim();

        if (field.length > 0) {

            if (field.indexOf("List<") > 0 && field.endsWith("s;")) {
                container.add("\t@Singular");
            }

            if ((field.indexOf("List<") > -1 && replaceWithList) || field.indexOf(" List<") > -1) {
                let declaration = line.split(" ");
                let replacedType = '';

                declaration.forEach(function (component) {
                        let pos = component.indexOf("List<");
                        if (pos > -1) {
                            console.log("Element contains list = ", component)
                            let listType = component.substring(component.indexOf("<") + 1, component.indexOf(">"));
                            /*Is List type can be created by builder ?*/
                            if (isPrimitiveType(listType)) {

                                replacedType += "List<" + listType + ">" + " ";
                            } else {

                                replacedType += "List<" + listType + "Supplier." + listType + "SupplierBuilder" + ">" + " ";
                            }

                        } else {

                            replacedType += component + " ";
                        }
                    }
                )
                container.add("\t" + replacedType);

            } else {
                let end = line.lastIndexOf(" ");
                let start = line.substring(0, end).lastIndexOf(" ");
                let variableName = line.substring(end);
                let type = line.substring(start, end).trim();
                if (isPrimitiveType(type)) {
                    container.add("\t" + line);
                } else {
                    container.add("\tprivate " + type + "Supplier." + type + "SupplierBuilder" + variableName + ";");
                }

            }

        }
    })
    container.add("");

    /*BEGIN Static builder BEGIN*/
    container.add("\tpublic static " + wrapperWithNamespace + "." + builderClass + " configuredBuilder() {\n")

    container.add("\t\treturn " + wrapperWithNamespace + ".builder() ")
    fieldListArray.forEach((line, index, v2, v3) => {
        let trimmedLine = line.trim();
        let lastIndex = v2.length === index + 1;
        let variableName = trimmedLine.split(" ");
        let name = variableName[variableName.length - 1].replace(";", "");
        if (name.length > 1) {
            console.log("name", name, "trimmedLine", trimmedLine);
            container.add("\t\t\t." + name + "(" + pickValueByType(variableName[variableName.length - 2], name) + ")"
                + (lastIndex ? ";" : ""));
        }

    })
    container.add("   \n\t }")

    /*END Static initialized() builder END*/
    container.add("");
    container.add("\tpublic static " + className + " initialized() {");
    container.add("\t\treturn configuredBuilder().build().get();");
    container.add("\t}");
    container.add("");

    /* BEGIN assertEqual */
    container.add("")
    container.add("\tpublic static void assertEqual(" + className + " expected, " + className + " actual){")
    fieldListArray.forEach(function (line) {
        let variableName = line.split(" ");
        let name = variableName[variableName.length - 1].replace(";", "").trim();
        let type = variableName[variableName.length - 2].replace(";", "").trim();

        let shortener = name.substr(0, 1);
        let methodName;
        if (type === "boolean") {
            methodName = "is" + shortener.toUpperCase() + name.substr(1);
        } else {
            methodName = "get" + shortener.toUpperCase() + name.substr(1);
        }

        container.add("\t\tAssertions.assertThat(expected." + methodName + "()).isEqualTo(actual." + methodName + "());")
    })
    container.add("\t}")
    container.add("")


    /*BEGIN Static initialized() BEGIN*/

    /*END END*/

    /*BEGIN get() BEGIN*/
    container.add("\t@Override")
    container.add("\tpublic " + className + " get() {\n")
    if (buildStrategy === 'builder') {
        container.add("\t\treturn  " + className + ".builder()")
        fieldListArray.forEach(function (line) {
            let variableName = line.split(" ");
            let name = variableName[variableName.length - 1].replace(";", "");
            let type = variableName[variableName.length - 2].replace(";", "");
            if (name.length > 1) {
                let isList = type.indexOf("List<") > -1;
                if (isList) {
                    let listType = type.substring(type.indexOf("<") + 1, type.indexOf(">"));
                    if (isPrimitiveType(listType)) {
                        container.add(`\t\t\t.${name}(${name})`); /*Set value as is*/
                    } else {
                        container.add(`\t\t\t.${name}(${name} != null ? ${name}.stream().map(${(name.substr(0,
                            1))} -> ${(name.substr(
                            0, 1))}.build().get()).collect(Collectors.toList()) : null)`);
                    }
                } else {
                    if (isPrimitiveType(type)) {
                        container.add("\t\t\t." + name + "(" + name + ")");
                    } else {
                        container.add("\t\t\t." + name + "(" + name + " !=null ? " + name + ".build().get() : null)");
                    }
                }
            }

        })
        container.add("\t\t\t.build();")
        container.add("\t}")

    } else if (buildStrategy === 'setters') {

        container.add("\t\t" + className + " result = new " + className + "();")

        fieldListArray.forEach(function (line) {
            let variableName = line.split(" ");
            let name = variableName[variableName.length - 1].replace(";", "");
            let type = variableName[variableName.length - 2].replace(";", "");
            if (name.length > 1) {
                let isList = type.indexOf("List<") > -1;
                let shortener = name.substr(0, 1);
                if (isList) {
                    let listType = type.substring(type.indexOf("<") + 1, type.indexOf(">"));
                    if (isPrimitiveType(listType)) {
                        container.add(`\t\tresult.set${shortener.toUpperCase()}${name.substr(1)} (${name});`);
                    } else {
                        container.add(`\t\tresult.set${shortener.toUpperCase()}${name.substr(
                            1)} (${name} != null ? ${name}.stream().map(${shortener} -> ${shortener}.build().get()).collect(Collectors.toList()) : null);`);
                    }

                } else {
                    if (isPrimitiveType(type)) {
                        container.add("\t\tresult.set" + shortener.toUpperCase() + name.substr(1) + "(" + name + ");");
                    } else {
                        container.add(
                            "\t\tresult.set" + shortener.toUpperCase() + name.substr(1) + "(" + name + " != null ? "
                            + name + ".build().get() : null );");
                    }
                }
            }

        })

        container.add("\t\treturn result; ")
        container.add("\t}")

    } else if (buildStrategy === 'constructor') {
        container.add("\t\t" + className + " result = new " + className + "(");
        let index = 0;
        fieldListArray.forEach(function (line) {
            let variableName = line.split(" ");
            let name = variableName[variableName.length - 1].replace(";", "");
            let type = variableName[variableName.length - 2].replace(";", "");

            if (index > 0) {
                container.add_part(",");
                container.newline();
            }

            if (name.length > 1) {
                let isList = type.indexOf("List<") > -1;
                let shortener = name.substr(0, 1);
                if (isList) {
                    let listType = type.substring(type.indexOf("<") + 1, type.indexOf(">"));
                    if (isPrimitiveType(listType)) {
                        container.add_part('\t\t\t' + name)
                    } else {
                        container.add_part(`\t\t\t(${name} != null ? ${name}.stream().map(${shortener} -> ${shortener}.build().get()).collect(Collectors.toList()) : null)`);
                    }
                } else {
                    if (isPrimitiveType(type)) {
                        container.add_part('\t\t\t' + name)
                    } else {
                        container.add_part("\t\t\t" + name + " !=null ? " + name + ".build().get() : null ");
                    }
                }
            }
            index++;

        })
        container.add("")
        container.add("\t\t);")

        container.add("\t\treturn result; ")
        container.add("\t}");
    }


    /*END unwrap END*/
    container.add("}")

    /*END class END*/
    return container;

}

function getRadioButtonValue(name = 'get_option') {
    let radios = document.getElementsByName(name);

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            return (radios[i].value);
        }
    }
}

function onGenerateClick() {
    let buildStrategy = getRadioButtonValue("get_option");

    let replaceWithListElement = document.getElementById("replace_with_list");
    let replaceWithList = replaceWithListElement.checked;

    let className = document.getElementById("id_pool_class_name");
    let targetClassName = className.value == null || className.value.trim().length === 0 ? "ClassNameNotSet"
        : className.value;

    let namespaceElement = document.getElementById("id_namespace");
    let namespace = namespaceElement.value == null || namespaceElement.value.trim().length === 0 ? ""
        : namespaceElement.value;

    let fields = document.getElementById("id_pool_fields");
    let wrapper = createWrapper(targetClassName, fields.value, new Maker(), buildStrategy, namespace, replaceWithList);

    let preResult = document.getElementById("id_pool_result");
    preResult.innerText = wrapper.getContent();
    copyToClip(wrapper.getContent())
}

let primitiveTypes = ["boolean", "Boolean", "Long", "long", "String", "BigDecimal", "BigInteger", "Long", "Double", "double",
    "Integer", "int", "LocalDateTime", "Instant", "LocalDate"];

// LocalDate l = LocalDate.of()

function formatCurrentDate() {
    const currentDate = new Date();

    // Format the date as "YYYY-MM-DDTHH:mm:ss.SSSZ"
    const formattedDate = currentDate.toISOString().slice(0, 19) + '.00Z';

    return formattedDate;
}


function pickValueByType(type, variableName) {
    const listTypeMarker = "List<";
    switch (type.trim()) {
        case "boolean":
        case "Boolean":
            return "true";
        case "Instant":
            return "Instant.parse(\"" + formatCurrentDate() + "\")"
        case "String":
            return '"' + variableName + '"';
        case "Long":
        case "long":
            return getCommonValueIndex() + "L";
        case "Double":
        case "double":
            return getCommonValueIndex() + ".0";
        case "Integer":
        case "int":
            return getCommonValueIndex();
        case "List<Integer>":
            return "Arrays.asList(" + getCommonValueIndex() + ", " + getCommonValueIndex() + ")";
        case "List<Long>":
            return "Arrays.asList(" + getCommonValueIndex() + "L, " + getCommonValueIndex() + "L)";
        case "List<String>":
            return "Arrays.asList(\"" + variableName + "_1\",\"" + variableName + "_2\")";
        case "BigDecimal":
            return "BigDecimal.valueOf(" + getCommonValueIndex() + ")";
        case "BigInteger":
            return "BigInteger.valueOf(" + getCommonValueIndex() + ")";
        case "LocalDateTime":
            return "LocalDateTime.of(" + new Date().getFullYear() + "," + new Date().getMonth() + ","
                + new Date().getDate() + "," + new Date().getHours() + "," + new Date().getMinutes() + "" + ")"
        case "LocalDate":
            return "LocalDate.of(" + new Date().getFullYear() + "," + new Date().getMonth() + "," + new Date().getDate() + ")";
        default :
            let isTypeList = type.indexOf(listTypeMarker) > -1;
            if (isTypeList) {
                let listType = type.substr(listTypeMarker.length, type.length - listTypeMarker.length - 1);
                return "Arrays.asList(" + listType + "Supplier.configuredBuilder())";

            } else {
                return type + "Supplier.configuredBuilder()";
            }
    }
}

let commonNumericIndex = 1;

function getCommonValueIndex() {
    commonNumericIndex++;
    return commonNumericIndex;
}

class Maker {

    constructor() {
        this.content = "";
    }

    add(message) {
        this.content = this.content + message + "\n";
    }

    add_part(message) {
        this.content = this.content + message;
    }

    newline() {
        this.content = this.content + "\n";
    }

    print() {
        console.log(this.content);
    }

    getContent() {
        return this.content;
    }
}

function trimContent(content) {
    let newContent = "";
    let first = true;
    content.value.split("\n").forEach(function (line) {

        if (line.trim().length > 0) {
            if (!first) {
                newContent += "\n";
            }
            first = false;
            newContent += line.trim();
        }
    })
    content.value = newContent;
}
